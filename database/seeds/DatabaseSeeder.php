<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name' => 'administrator',
        ]);

        Role::create([
            'name' => 'author',
        ]);

        Role::create([
            'name' => 'subscriber',
        ]);

        User::create([
            'role_id' => 1,
            'status' => 1,
            'name' => 'Arjet Kraja',
            'email' => 'arjeti.kraja@gmail.com',
            'password' => bcrypt('123456'),
        ]);

        User::create([
            'role_id' => 2,
            'status' => 1,
            'name' => 'Ben Stiller',
            'email' => 'ben@email.com',
            'password' => bcrypt('123456'),
        ]);

        User::create([
            'role_id' => 3,
            'status' => 0,
            'name' => 'Dwayne Johnson',
            'email' => 'rock@email.com',
            'password' => bcrypt('123456'),
        ]);
    }
}
